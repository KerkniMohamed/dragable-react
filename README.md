React is a JavaScript library for building user interfaces.

Declarative: React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes. Declarative views make your code more predictable, simpler to understand, and easier to debug.
Component-Based: Build encapsulated components that manage their own state, then compose them to make complex UIs. Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.
Learn Once, Write Anywhere: We don't make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code. React can also render on the server using Node and power mobile apps using React Native.
Learn how to use React in your own project.

Installation
React has been designed for gradual adoption from the start, and you can use as little or as much React as you need:

Use Online Playgrounds to get a taste of React.
Add React to a Website as a <script> tag in one minute.
Create a New React App if you're looking for a powerful JavaScript toolchain.
You can use React as a <script> tag from a CDN, or as a react package on npm.

Documentation
You can find the React documentation on the website.

Check out the Getting Started page for a quick overview.

The documentation is divided into several sections:

Tutorial
Main Concepts
Advanced Guides
API Reference
Where to Get Support
Contributing Guide
You can improve it by sending pull requests to this repository.


